package com.microshopper.apigateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiGatewayRoutesConfiguration {

  @Bean
  public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
    return builder.routes()
      .route(r -> r.path("/api/products/**")
        .uri("lb://product-service/")
        .id("product-service"))
      .route(r -> r.path("/api/categories/**")
        .uri("lb://product-service/")
        .id("product-service"))
      .route(r -> r.path("/api/accounts/**")
        .uri("lb://account-service/")
        .id("account-service"))
      .route(r -> r.path("/api/product-reviews/**")
        .uri("lb://product-review-service/")
        .id("product-review-service"))
      .route(r -> r.path("/api/manufacturers/**")
        .uri("lb://manufacturer-service/")
        .id("manufacturer-service"))
      .route(r -> r.path("/api/product-catalog/**")
        .uri("lb://product-catalog-service/")
        .id("product-catalog-service"))
      .route(r -> r.path("/api/notifications/**")
        .uri("lb://notification-service/")
        .id("notification-service"))
      .route(r -> r.path("/api/orders/**")
        .uri("lb://order-service/")
        .id("order-service"))
      .route(r -> r.path("/oauth/**")
        .uri("lb://auth-service/")
        .id("auth-service"))
      .build();
  }
}
