package com.microshopper.apigateway.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

import java.security.interfaces.RSAPublicKey;

@Configuration
@EnableWebFluxSecurity
public class OAuth2ResourceServerConfig {

  @Value("classpath:jwt-microshopper-platform.pub")
  RSAPublicKey publicKey;

  @Bean
  public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
    http.authorizeExchange()
      .pathMatchers("/oauth/**").permitAll()
      .pathMatchers(HttpMethod.POST, "/api/accounts").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/products").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/products/*").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/products/search**").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/products/manufacturer/*").permitAll()
      .pathMatchers(HttpMethod.POST, "/api/products").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/categories").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/categories/*").permitAll()
      .pathMatchers(HttpMethod.POST, "/api/categories").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/manufacturers").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/manufacturers/*").permitAll()
      .pathMatchers(HttpMethod.POST, "/api/manufacturers").permitAll()
      .pathMatchers(HttpMethod.POST, "/api/manufacturers/accounts").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/product-reviews/").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/product-reviews/product/*").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/product-catalog/**").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/notifications/**").permitAll()
      .pathMatchers(HttpMethod.GET, "/api/orders/track/*").permitAll()
      .anyExchange()
      .authenticated()
      .and()
      .csrf()
      .disable()
      .oauth2ResourceServer()
      .jwt()
      .jwtDecoder(jwtDecoder());

    return http.build();
  }

  @Bean
  public ReactiveJwtDecoder jwtDecoder() {
    return NimbusReactiveJwtDecoder.withPublicKey(publicKey).build();
  }
}
